import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

axios.defaults.baseURL = process.env.VUE_APP_ROOT_API

console.log(process.env.VUE_APP_ROOT_API + 'here')

export default new Vuex.Store({
  state: {
    user: null,
    token: null,
    steps: null
  },
  mutations: {
    setToken (state, token) {
      state.token = token
      localStorage.setItem('token', JSON.stringify(token))
      // all requests now have this header
      axios.defaults.headers.common.Authorization = `Bearer ${token}`
    },

    setUserData (state, userData) {
      state.user = userData
      localStorage.setItem('user', JSON.stringify(userData))
    },

    setUserSteps (state, steps) {
      state.steps = steps
    },

    addUserSteps (state, stepsObject) {
      state.steps.push(stepsObject)
    },

    removeStep (state, id) {
      const index = state.steps.findIndex(step => step.id == id)
      state.steps.splice(index, 1)
    },

    clearUserData () {
      localStorage.removeItem('token')
      location.reload()
    }
  },
  actions: {
    login ({ commit, dispatch }, credentials) {
      return axios
        .post('/auth/login', credentials)
        .then(data => {
          commit('setToken', data.data._authToken)
          dispatch('getCurrentUser')
        })
    },
    register ({ commit, dispatch }, userData) {
      return axios
        .post('/user', userData)
        .then(data => {
          commit('setToken', data.data._authToken)
          dispatch('getCurrentUser')
        })
    },
    getCurrentUser ({ commit }) {
      return axios
        .get('/auth/me')
        .then(({ data }) => {
          commit('setUserData', data)
        })
    },
    getUserSteps ({ commit }) {
      return axios
        .get('/auth/steps')
        .then(data => {
          commit('setUserSteps', data.data)
        })
    },
    postUserSteps ({ commit }, stepData) {
      return axios
        .post('/auth/steps', stepData)
        .then(data => {
          console.log(data)
          commit('addUserSteps', data.data)
        })
    },
    removeUserSteps ({ commit }, data) {
      return axios.delete('/auth/remove_steps/' + data.id)
        .then(() => {
          commit('removeStep', data.id)
        })
    },
    logout ({ commit }) {
      commit('clearUserData')
    }
  },
  getters: {
    isLogged: state => !!state.token,
    user: state => state.user,
    steps: state => state.steps
  },

  modules: {}
})
