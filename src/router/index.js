import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Dashboard from '../views/Dashboard.vue'
import About from '../views/About.vue'
import Features from '../views/Features.vue'
import Steps from '../views/Steps.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  component: Home
},
{
  path: '/dashboard',
  component: Dashboard,
  meta: {
    auth: true
  }
},
{
  path: '/login',
  component: Login
},
{
  path: '/register',
  component: Register
},
{
  path: '/features',
  component: Features
},
{
  path: '/steps',
  component: Steps
},
{
  path: '/about',
  component: About
}
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const loggedIn = localStorage.getItem('token')

  if (to.matched.some(record => record.meta.auth) && !loggedIn) {
    next('/login')
    return
  }
  next()
})

export default router
