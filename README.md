# Walker - Sample Vue CLI and Xano App

## Project setup

```
Create a .env file in the main directory
Then put in the following: VUE_APP_ROOT_API=your-unique-xano-base-request-url
```

```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
